# CI/CD with Microsoft VSTS
Lab 01: Deploy an Agent on Windows

---

# Preparations
  
 - Get build server IP (ask the instructor)
 
 - Credentials: sela:sela

---

# Tasks

 - Create an Azure DevOps organization
 
 - Create a Team Project for the workshop
 
 - Download the Agent and create a new Agent Pool
 
 - Create a Personal Access Token (PAT)
 
 - Configure an Agent
 
 - Install npm and add a capability

--- 
 
## Create an Azure DevOps organization

&nbsp;

 - Browse to the Azure DevOps main page and login:

```
https://dev.azure.com
```

&nbsp;

 - Create a new organization by clicking "New Organization":
 

<img alt="Image 1.1" src="images/task-1.1.png"  width="75%" height="75%">

&nbsp;

 - Click "Continue" to accepts the terms and conditions:
 

<img alt="Image 1.2" src="images/task-1.2.png"  width="75%" height="75%">

&nbsp;

 - Set a name and a region for your Azure DevOps organization:
 

<img alt="Image 1.3" src="images/task-1.3.png"  width="75%" height="75%">

&nbsp;

 - Wait until the organization is ready  


&nbsp;

---

## Create a Team Project for the workshop

&nbsp;

 - Browse to the organization main page:

```
https://dev.azure.com/<your-organization>
```
 
 &nbsp;
 
 - Create a new project called "ci-cd-workshop":
 

<img alt="Image 2.1" src="images/task-2.1.png"  width="75%" height="75%">

&nbsp;

 - Ensure the project was successfully created:
 

<img alt="Image 2.2" src="images/task-2.2.png"  width="75%" height="75%">

&nbsp;
 
---

## Download the Agent and create a new Agent Pool

 - Browse to the "Agent Pools" page:

```
https://<organization>.visualstudio.com/<project>/_settings/agentqueues
```

 - Download the agent by click "Download Agent", Select "Windows" and click "Download"
  

<img alt="Image 3.1" src="images/task-3.1.png"  width="75%" height="75%">

 - Extract the zip file in the location below:

```
$ C:\Agent
```

 - Create a new Pool called "Sela" by click "New Pool" in the "Agent Pools" page
 

<img alt="Image 3.2" src="images/task-3.2.png"  width="75%" height="75%">


---

## Create a Personal Access Token (PAT)

 - Open your profile and go to your security details:

 ![Image 1](https://docs.microsoft.com/en-us/azure/devops/repos/git/_shared/_img/my-profile-team-services.png?view=vsts)

 - Select "Personal Access Token" and click "Add" to create the token:

 ![Image 2](https://docs.microsoft.com/en-us/azure/devops/repos/git/_shared/_img/add-personal-access-token.png?view=vsts)

 - Create your token (default values): 

 ![Image 3](https://docs.microsoft.com/en-us/azure/devops/repos/git/_shared/_img/setup-personal-access-token.png?view=vsts)

 - When you're done, make sure to copy the token
 
---

## Configure an Agent

 - Open Powershell as Administrator and move to the agent folder:

```
$ cd C:\Agent
```

 - Start the configuration by run:

```
$ .\config.cmd
```

 - Enter server URL:

```
$ https://<instance>.visualstudio.com
```

 - Enter authentication type:

```
$ PAT
```

 - Enter personal access token:

```
$ <Personal-Access-Token>
```

 - Enter agent pool:

```
$ Sela
```

 - Enter agent name:

```
$ Sela-Agent
```

 - Enter work folder:

```
$ _work
```

 - Enter run agent as service?:

```
$ y
```

 - Enter User account to use for the service:

```
$ sela
```

 - Browse to the "Agent Pools" page and ensure the Agent was created and is enable

 <img alt="Image 4.1" src="images/task-4.1.png"  width="75%" height="75%">
 
---

## Install npm and add a capability

 - Let's inspect the agent capabilities in the "Agent Pools" Page (check that npm is not available)
 

<img alt="Image 5.1" src="images/task-5.1.png"  width="75%" height="75%">


 - Install npm LTS (installer downloaded in the desktop) or download from:

```
https://nodejs.org/en/
```

 - Open the terminal and ensure npm was installed:

```
$ npm -v
```

 - Let's inspect the agent capabilities again to see if nodejs and npm were added to the list (is not added)

 - Open "services" in the build machine and restart the "VSTS Agent" service
 
 ![Image 5](images/lab01-5.png)

 - Let's inspect the agent capabilities again (refresh the page) and you will see npm in the capabilities list
  

 <img alt="Image 5.2" src="images/task-5.2.png"  width="75%" height="75%">


